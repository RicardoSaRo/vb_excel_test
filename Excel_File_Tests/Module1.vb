﻿Imports ClosedXML.Excel

Module Module1

    Sub Main()
        Dim filepath As String = "C:\Users\Ricardo\Desktop\PR.xlsx"
        Dim wbook = New XLWorkbook(filepath)
        Dim ws = wbook.Worksheet(1)
        Dim data = ws.Cell("B7").GetValue(Of String)

        'Writes cell A1 with TEST
        ws.Cell("A1").Value = "TEST"
        wbook.Save()

        'Shows cell B7
        Console.WriteLine(data)

        'Looks into the Used Cells and Finds Cells containing the search criteria
        Dim list = New List(Of IXLCell)
        Dim criteria As String = "1"
        For Each usedcell As IXLCell In ws.CellsUsed
            If usedcell.Value.ToString().Contains(criteria) Then
                list.Add(usedcell)
            End If
        Next

        'Prints the searched criteria on the list
        For Each item In list
            If item.Value.ToString() <> String.Empty Then
                Console.WriteLine(item.Value.ToString())
            End If
        Next

    End Sub

End Module
